jQuery(document).ready(function ($) {

    /* close modal */
    $('span[data-close="modal"]').click(function () {

        $("#modal").fadeOut();

        var modal = $('#modal-body');

        modal.removeClass('in');
        modal.empty();
    });

    /* commenting */
    $('#post').click(function (e) {

        btnPost = $(this);
        btnPost.html('<i class="fa fa-circle-o-notch fa-spin"></i>');

        $.post(
            $('#comment-form').attr("data-url"),
            {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'message': $("#comment-form .input-group .form-control").val()
            },
            function (data) {

                $("#comment-form .input-group .form-control").val("");

                $("#comment-form").after(data);

                btnPost.html('post');
            }
        );
    });

    /* liking */
    $('#like').click(function (e) {

        e.preventDefault();

        likes = parseInt($('#likes').text());

        $.post(
            $(this).attr("href"),
            {
                '_token': $('meta[name="csrf-token"]').attr('content'),
            })
            .done(function (data) {

                likes += 1;

                $('#likes').text(likes);
        });
    });
});
