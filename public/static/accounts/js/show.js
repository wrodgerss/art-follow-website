jQuery(document).ready(function ($) {
    
    /* show modal */
    $('#content #workfolios .workfolio a[data-show="modal"]').click(function (e) {

        e.preventDefault();

        $('#preload').css('display', 'block');

        var modal = $('#modal-body');
        var anchor = $(e.currentTarget);

        setTimeout(function () {
            $.ajax(anchor.context.href).success(function (data) {

                $("#modal").fadeIn();

                modal.html(data);
                modal.addClass('in');
            })
            .complete(function () {

                $('#preload').css('display', 'none');
            });
        }, 6000);
    });
});
    