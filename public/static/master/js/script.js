jQuery(document).ready(function ($) {

    /* navbar toggle */
    $('.navbar .container-fluid .navbar-header button.navbar-toggle:first-child').click(function () {

        $('#aside').toggleClass('in');
    });

    $('.navbar .container-fluid .navbar-header button.navbar-toggle:last-child').click(function () {

        $('.navbar .container-fluid .navbar-header .navbar-form').addClass('show-search');
        
        $('.navbar').css('visibility', 'hidden');
    });

    /* active link */
    path = window.location.href;
    tag = '#aside a[href="' + path + '"], #aside a[href="' + path + '"] span';
    anchor = $(tag);

    anchor.css('background', '#e8e8e8');

    /* sign out */
    $('#signout').click(function () {

        $('#signout-form').submit();
    });
})
