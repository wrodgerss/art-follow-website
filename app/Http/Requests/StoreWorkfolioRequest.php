<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWorkfolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'cover' => 'required|mimetypes:image/*',
            'file_upload' => 'required|mimetypes:image/*,video/*,audio/*',
            'file_type' => 'required|in:image,video,audio',
            'tags' => 'nullable|string'
        ];
    }
}
