<?php

namespace App\Http\Controllers;

use App\Workfolio;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StoreWorkfolioRequest;
use Auth;
use App\Http\Requests\EditWorkfolioRequest;
use Session;
use Image;
use Storage;
use DB;
use App\Tag;

class WorkfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workfolios = Workfolio::latest()->paginate(9);

        return view('workfolios.index', compact('workfolios'));
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @param string $type
     * @return \Illuminate\Http\Response
     */
    public function filter($type)
    {
        $workfolios = Workfolio::where('file_type', $type)->paginate(9);

        return view('workfolios.index', compact('workfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workfolios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkfolioRequest $request)
    {
        $user = Auth::user();

        $cover = $request->file('cover')->store('workfolios/'.$user->id, 'public');

        $file_upload = $request->file('file_upload')->store('workfolios/'.$user->id, 'public');

        $workfolio = $user->workfolios()->create([
            'title' => $request->title,
            'description' => $request->description,
            'cover' => $cover,
            'file_upload' => $file_upload,
            'file_type' => $request->file_type,
        ]);

        $tags = explode(',', $request->tags);

        foreach($tags as $tag)
        {
            $tag = Tag::firstOrCreate([
                'tag' => $tag
            ]);

            $workfolio->tags()->attach($tag->id);
        }

        return redirect()->route('workfolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  var int
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workfolio = Workfolio::with(['user', 'comments', 'likes'])->find($id);

        $workfolio->views()->firstOrCreate(
            
            ['user_id' => Auth::user()->id]
        );

        $rating = DB::table('ratings')
                    ->where('workfolio_id', $workfolio->id)
                    ->avg('rating');
        
        return view('workfolios.show', [
            'workfolio' => $workfolio, 'rating' => $rating
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workfolio  $workfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(Workfolio $workfolio)
    {
        return view('workfolios.edit', compact('workfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workfolio  $workfolio
     * @return \Illuminate\Http\Response
     */
    public function update(EditWorkfolioRequest $request, Workfolio $workfolio)
    {
        $cover = $workfolio->cover;

        $file_upload = $workfolio->file_upload;

        if ($request->hasFile('cover') && $request->file('cover')->isValid())
        {
            Storage::delete($cover);

            $cover = $request->file('cover')->store('workfolios/'.$workfolio->user->id, 'public');
        }

        if ($request->hasFile('file_upload') && $request->file('file_upload')->isValid())
        {
            Storage::delete($file_upload);

            $file_upload = $request->file('file_upload')->store('workfolios/'.$workfolio->user->id, 'public');
        }

        $workfolio->update([
            'title' => $request->title,
            'description' => $request->description,
            'cover' => $cover,
            'file_upload' => $file_upload,
            'file_type' => $request->file_type
        ]);

        Session::flash('message', 'your changes have been saved');

        return redirect()->route('workfolio.edit', $workfolio->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workfolio  $workfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workfolio $workfolio)
    {
        $workfolio->delete();

        return redirect()->route('workfolio.index');
    }

    /**
    * Save a comment for the workfolio
    * 
    * @param \Illuminate\Http\Request $request
    * @param \App\Workfolio $workfolio
    * @return \Illuminate\Http\Response
    */
    public function comment(Request $request, Workfolio $workfolio)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $comment = $workfolio->comments()->create([
            'message' => $request->message,
            'user_id' => Auth::user()->id,
        ]);

        return view('workfolios.partials.comment', compact('comment'));
    }

    /**
    * like a workfolio
    * 
    * @param \App\Workfolio $workfolio
    * @return \Illuminate\Http\Response
    */
    public function like(Workfolio $workfolio)
    {
        $like = $workfolio->likes()->firstOrCreate(

            ['user_id' => Auth::user()->id]
        );

        if ($like->wasRecentlyCreated)
        {
            return response('Thank you for liking', 200);
        }
        else
        {
            return response('Your already liked this', 403);
        }
    }

    /**
    * perform a search
    * 
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function search(Request $request)
    {
        $this->validate($request, [
            'lookup' => 'required'
        ]);

        $lookup = $request->lookup;

        $workfolios = Workfolio::with(['user'])
                                ->where('title', 'LIKE', '%'.$lookup.'%')
                                ->orWhere('description', 'LIKE', '%'.$lookup.'%')
                                ->get();

        $users = User::where('username', 'LIKE', '%'.$lookup.'%')->get();

        $videos = $workfolios->filter(function ($item) {

            return $item->file_type == 'video';
        });

        $audios = $workfolios->filter(function ($item) {

            return $item->file_type == 'audio';
        });

        $images = $workfolios->filter(function ($item) {

            return $item->file_type == 'image';
        });

        return view('workfolios.search', [

            'users' => $users,
            'videos' => $videos,
            'images' => $images,
            'audios' => $audios,
            'lookup' => $lookup
        ]);
    }

    /**
    * Rate the workfolio
    * 
    * @param \Illuminate\Http\Request $request
    * @param int $workfolio
    * @return \Illuminate\Http\Response
    */
    public function rating(Request $request, $workfolio)
    {
        $this->validate($request, [
            'rating' => 'required|numeric'
        ]);

        $user = $request->user()->id;

        if(!DB::table('ratings')->where('workfolio_id', $workfolio)->where('user_id', $user)->exists())
        {
            DB::table('ratings')->insert([
                'workfolio_id' => $workfolio,
                'user_id' => $user,
                'rating' => $request->rating
            ]);
        }

        return back();
    }
}
