<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workfolio;
use DB;

class UserTaggingController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Workfolio $workfolio
     * @return \Illuminate\Http\Response
     */
    public function create(Workfolio $workfolio)
    {
        return view('workfolio.tagged_users.create', compact('workfolio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Workfolio $workfolio
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Workfolio $workfolio)
    {
        $this->validate($request, [
            'user' => 'exists:users,id',
            'role' => 'nullable'
        ]);

        if(!DB::table('user_workfolio')->where('user_id', $request->user)->where('workfolio_id', $workfolio->id)->exists())
        {
            $workfolio->participants()->attach($request->user, [
                'role' => $request->role
            ]);
        }

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workfolio $workfolio
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Workfolio $workfolio, User $user)
    {
        $role = DB::table('user_workfolio')->where('user_id', $user->id)->where('workfolio_id', $workfolio->id)->get('role');

        return view('workfolio.tagged_users.edit', [
            'role' => $role,
            'user' => $user,
            'workfolio' => $workfolio
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workfolio $workfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workfolio $workfolio)
    {
        $this->validate($request, [
            'user' => 'exists:user_workfolio,user_id',
            'role' => 'nullable'
        ]);

        $workfolio->participants()->updateExistingPivot($request->user, [
            'role' => $request->role
        ]);

        return redirect()->route('workfolio.show', $workfolio);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workfolio  $workfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workfolio $workfolio)
    {
        $this->validate($request, [
            'user' => 'exists:user_workfolio,user_id'
        ]);

        $workfolio->participants()->detach($request->user);

        return redirect()->route('workfolio.show', $workfolio->id);
    }
}
