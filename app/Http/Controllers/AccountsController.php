<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Auth;
use Storage;
use Hash;
use Session;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('workfolios')->get();

        return view('accounts.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  $username
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::with(['profile', 'workfolios', 'favourites', 'connections'])
                    ->where('username', $username)->first();

        $ratings = 0;

        $user->workfolios->each(function ($item, $key) {

            $ratings += DB::table('ratings')
                            ->where('workfolio_id', $workfolio->id)
                            ->avg('rating');
        });

        $rating = (int) ($ratings / $user->workfolios->count());

        return view('accounts.show', [
            'user' => $user, 'rating' => $rating
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();

        return view('accounts.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $profile = $user->profile;

        $avatar = $profile->avatar;

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
        {
            Storage::delete($avatar);

            $avatar = $request->file('avatar')->store('profiles/'.$user->id, 'public');
        }

        $profile->update([

            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birth_day' => $request->birth_day,
            'bio' => $request->bio,
            'country' => $request->country,
            'city' => $request->city,
            'gender' => $request->gender,
            'avatar' => $avatar
        ]);

        return redirect()->route('accounts.show', $user->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Auth::user()->delete();

        return redirect()->route('login');
    }

    /**
     * change user email
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function change_email(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users'
        ]);

        $request->user()->update(['email' => $request->email]);

        Session::flash('message', 'Your email has been updated');

        return redirect()->route('accounts.edit');
    }

    /**
     * change user password
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function change_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6'
        ]);

        $request->user()->update(['password' => Hash::make($request->password)]);

        Session::flash('message', 'Your password has been updated');

        return redirect()->route('accounts.edit');
    }

    public function connection($username)
    {
        $user = User::where('username', $username)->first();

        $user->connections()->toggle(Auth::user()->id);

        return redirect()->route('accounts.show', $username);
    }

    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(10)->get()->toArray();
    }
   
}
