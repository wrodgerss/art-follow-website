<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\SocialProvider;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Where to redirect users after login.
     */
    public function redirectTo()
    {
        return route('workfolio.index');
    }

    /**
    * Redirect the user to the Facebook authentication page.
    *
    * @return Response
    */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
    * Obtain the user information from Facebook.
    *
    * @return Response
    */
    public function handleProviderCallback($provider)
    {
        try
        {
            $social_user = Socialite::driver($provider)->user();
        }
        catch (\Exception $e)
        {
            return back();
        }

        $social_provider = SocialProvider::where('provider_id', $social_user->getId())->first();

        if(!$social_provider)
        {
            // create new user and social provider
            $user = User::firstOrCreate([
                'email' => $social_user->getEmail(),
                'username' => $social_user->getName(),
            ]);

            $user->socialProviders()->create([
                'provider_id' => $social_user->getId(),
                'provider' => $provider
            ]);
        }
        else
            $user = $social_provider->user;

        auth()->login($user);

        if($user->wasRecentlyCreated)
        {
            // setup profile
        }

        redirectTo();
    }

}
