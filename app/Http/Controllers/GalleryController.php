<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workfolio;
use App\Gallery;
use Storage;

class GalleryController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Workfolio $workfolio
     * @return \Illuminate\Http\Response
     */
    public function create(Workfolio $workfolio)
    {
        return view('workfolio.gallery.create', compact('workfolio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Workfolio $workfolio)
    {
        $this->validate($request, [
            'image' => 'required|mimetypes:image/*',
            'description' => 'nullable',
            'timestamp' => 'nullable'
        ]);

        $image = $request->file('image')->store('workfolios/'.$user->id, 'public');

        $workfolio->gallery()->create([
            'image' => $image,
            'description' => $request->description,
            'timestamp' => $request->timestamp
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        return view('workfolio.gallery.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        return view('workfolio.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        $image = $gallery->image;

        if($request->hasFile('image') && $request->file('image')->isValid())
        {
            Storage::delete($image);

            $image = $request->file('image')->store('workfolios/'.$request->user()->id, 'public');
        }

        $gallery->update([
            'image' => $image,
            'description' => $request->description,
            'timestamp' => $request->timestamp,
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $workfolio = $gallery->workfolio->id;

        $gallery->delete();

        return redirect()->route('workfolio.show', $workfolio);
    }
}
