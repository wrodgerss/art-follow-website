<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Workfolio;

class WorkfolioUpload extends Mailable
{
    use Queueable, SerializesModels;

    public $workfolio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Workfolio $workfolio)
    {
        $this->workfolio = $workfolio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('workfolio.mail.workfolio_upload');
    }
}
