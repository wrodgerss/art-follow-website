<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class AccountRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * represents the user instance to receive the mail
    *
    * @var \App\User
    */
    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('accounts.mail.account_registration')
                    ->with(['user' => $this->user]);
    }
}
