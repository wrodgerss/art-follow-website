<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function workfolios()
    {
        return $this->hasMany(Workfolio::class);
    }

    public function favourites()
    {
        return $this->belongsToMany(Workfolio::class, 'likes');
    }

    public function connections()
    {
        return $this->belongsToMany(self::class, 'connections', 'following_id', 'follower_id');
    }

    public function tagged_workfolios()
    {
        return $this->belongsToMany(Workfolio::class)->withPivot('role');
    }

    public function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }
}
