<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\User;
use App\Profile;
use App\Workfolio;
use App\Observers\UserObserver;
use App\Observers\ProfileObserver;
use App\Observers\WorkfolioObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        User::observe(UserObserver::class);

        Profile::observe(ProfileObserver::class);

        Workfolio::observe(WorkfolioObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
