<?php

namespace App\Observers;

use App\Profile;
use App\User;
use Mail;
use App\Mail\AccountRegistration;
use App\Mail\AccountUpdate;

class UserObserver
{
    public function created(User $user)
    {
        $user->profile()->create([]);

        Mail::to($user->email)
            ->queue(new AccountRegistration($user));
    }

    public function updated(User $user)
    {
        Mail::to($user->email)
            ->queue(new AccountUpdate($user));
    }
}
