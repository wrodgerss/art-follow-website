<?php

namespace App\Observers;

use App\Workfolio;
use Storage;
use App\Mail\WorkfolioUpload;
use Mail;
use Auth;
use App\Notifications\WorkfolioUpload as NotifyWorkfolioUpload;

class WorkfolioObserver
{
    public function deleting(Workfolio $workfolio)
    {
        Storage::delete($workfolio->cover);

        Storage::delete($workfolio->file_upload);
    }

    public function created(Workfolio $workfolio)
    {
        $user = Auth::user();

        // $user->notify(new NotifyWorkfolioUpload($workfolio));

        foreach($user->connections as $connection)
        {
            if($connection->profile->subscription)
            {
                Mail::to($connection->email)
                ->queue(new WorkfolioUpload($workfolio));
            }
        }
    }
}
