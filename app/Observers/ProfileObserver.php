<?php

namespace App\Observers;

use App\Profile;
use Storage;

class ProfileObserver
{
    public function deleting(Profile $profile)
    {
        Storage::delete($profile->avatar);
    }
}
