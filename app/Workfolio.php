<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Workfolio extends Model
{
    protected $fillable = [
        'title', 'description', 'cover', 'file_upload', 'file_type'
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function views()
    {
        return $this->hasMany(View::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cover_url()
    {
        return asset(Storage::url($this->cover));
    }

    public function file_url()
    {
        return asset(Storage::url($this->file_upload));
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function participants()
    {
        return $this->belongsToMany(User::class)->withPivot('role');
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }
}
