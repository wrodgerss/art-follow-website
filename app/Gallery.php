<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'image', 'description', 'timestamp'
    ];

    public function workfolio()
    {
        return $this->belongsTo(Workfolio::class);
    }
}
