<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use Carbon\Carbon;

class Profile extends Model
{
    protected $fillable = [
        'avatar', 'bio', 'country', 'city', 'first_name', 'last_name', 'gender', 'birth_day'
    ];

    protected $dates = ['birth_day'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function birthdate()
    {
        $birthday = $this->birth_day;

        if ($birthday == null)
        {
            return;
        }

        $date = new Carbon($birthday);

        return $date->format('Y-m-d');
    }

    public function avatar_url()
    {
        $path = $this->avatar;

        if ($path == null)
        {
            return asset('images/avatar.png');
        }

        return asset(Storage::url($path));
    }

    public function full_name()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function location()
    {
        return $this->country.', '.$this->city;
    }

    public function age()
    {
        return Carbon::now()->diffInYears($this->birth_day);
    }
}
