<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Workfolio;

class WorkfolioUpload extends Notification implements ShouldQueue
{
    use Queueable;

    protected $workfolio;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Workfolio $workfolio)
    {
        $this->workfolio = $workfolio;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'workfolio_id' => $this->workfolio->id,
            'workfolio_title' => $this->workfolio->title,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'workfolio_id' => $this->workfolio->id,
            'workfolio_title' => $this->workfolio->title,
        ];
    }
}
