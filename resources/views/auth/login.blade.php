@extends('auth.master')

@section('title', 'sign in')

@section('content')
    <h1 id="title">login</h1>
    <form action="{{ route('login') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" class="form-control" name="username" placeholder="username">
            </div>
            @if($errors->has('username'))
                {{ $errors->first('username') }}
            @endif
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password" placeholder="password">
                <span class="input-group-addon" id="forgot-password">
                    <a href="">
                        Forgot password?
                    </a>
                </span>
            </div>
            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif
        </div>
        <div class="checkbox">
            <label>
            <input type="checkbox" name="remember"> Remember me
            </label>
        </div>
        <button type="submit" class="form-control">
            sign in
        </button>
        <hr>
        Don't have an account? 
        <a href="{{ route('register') }}">
            sign up
        </a>
        here
    </form>
@endsection
