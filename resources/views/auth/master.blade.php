<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- x-csrf-token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Art-follow | @yield('title')</title>
        <!-- jquery -->
        <script src="{{ asset('vendor/jquery.js') }}"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- font awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- css -->
        <link rel="stylesheet" href="{{ asset('static/auth/css/forms.css') }}">
        @yield('css')
    </head>

    <body>
        @yield('content')
    </body>

</html>