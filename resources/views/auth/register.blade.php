@extends('auth.master')

@section('title', 'sign up')

@section('content')
    <h1 id="title">register</h1>
    <form action="{{ route('register') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" class="form-control" name="username" placeholder="username">
            </div>
            @if($errors->has('username'))
                {{ $errors->first('username') }}
            @endif
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                </span>
                <input type="email" class="form-control" name="email" placeholder="email">
            </div>
            @if($errors->has('email'))
                {{ $errors->first('email') }}
            @endif
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password" placeholder="password">
            </div>
            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password_confirmation" placeholder="confirm password">
            </div>
        </div>
        <button type="submit" class="form-control">
            sign up
        </button>
        <hr>
        Already have an account? 
        <a href="{{ route('login') }}">
            sign in
        </a>
        here
    </form>
@endsection
