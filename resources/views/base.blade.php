<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Art-follow</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- jquery -->
        <script src="{{ asset('vendor/jquery.js') }}"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- font awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- css -->
        <link rel="stylesheet" href="{{ asset('static/base.css') }}">
        @yield('css')
    </head>

    <body>
        <!-- navigation -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand">
                        Art-foll<span>o</span>w
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{ url('/login') }}">sign in</a>
                        </li>
                        <li>
                            <a href="{{ url('/register') }}">sign up</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- content -->
        @yield('content')
        <!-- footer links -->
        <ul id="footer-links" class="clearfix">
            <li>
                <a href="http://">
                    home
                </a>
            </li>
            <li>
                <a href="http://">
                    about us
                </a>
            </li>
            <li>
                <a href="http://">
                    term and conditions
                </a>
            </li>
            <li>
                <a href="http://">
                    help
                </a>
            </li>
        </ul>
        <!-- footer -->
        <div id="footer">
            <h5>
                &copy; {{ date('Y') }} Art-foll<span>o</span>w
            </h5>
        </div>
    </body>
</html>
