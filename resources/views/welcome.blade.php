@extends('base')

@section('css')
    <style>
        #jumbotron {
            background: linear-gradient(rgba(34, 34, 34, .7), #000), url({{ asset('images/background.png') }});
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>
@endsection

@section('content')
    <!-- try us -->
    <div id="jumbotron">
        <h1>
            connect with a wide platform of art
        </h1>
    </div>
    <!-- what up -->
    <div id="whats-up">
        <h3>
            Whats up with art-follow
        </h3>
        <ul class="clearfix">
            <li>
                <div class="cover">
                    <i class="fa fa-music"></i>
                </div>
                <h5>music</h5>
                <p>
                    join us to listen and stream to a wide range of audio uploads
                </p>
            </li>
            <li>
                <div class="cover">
                    <i class="fa fa-film"></i>
                </div>
                <h5>video</h5>
                <p>
                    join us to watch,stream and enjoy a wide range of video uploads.
                </p>
            </li>
            <li>
                <div class="cover">
                    <i class="fa fa-file-image-o"></i>
                </div>
                <h5>images</h5>
                <p>
                    upload and view many of the images within user galleries.
                </p>
            </li>   
            <li>
                <div class="cover">
                    <i class="fa fa-briefcase"></i>
                </div>
                <h5>workfolio</h5>
                <p>
                    here one is able to create and share projects and work that they are working on. 
                </p>
            </li>
        </ul>
        <p id="join">
            <a href="{{ route('register') }}">
                join us
            </a>
        </p>
    </div>
@endsection
