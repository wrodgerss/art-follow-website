@extends('base')

@section('css')
    <style>
        h1, h4 {
            color: #ccc;
        }
        h1 {
            margin-bottom: 60px;
        }
        h4 {
            margin-bottom: 15px;
            margin-top: 40px;
        }
        p {
            color: #888;
        }
        #content {
            padding: 60px 15% 60px 15%;
        }
        @media (max-width: 767px) {
            #content {
                padding-right: 15px;
                padding-left: 15px;
            } 
        }
    </style>
@endsection

@section('content')
    <div id="content">
        <h1>Terms and Conditions</h1>
        <h4>Acceptance of Terms and Conditions and Amendments</h4>
        <p>
            Each time you use or cause access to www.art-follow.com , you agree to be bound by the Terms and
            Conditions listed below, as amended from time to time with or without notice. In addition, if you are using
            a particular service on or through this www.art-follow.com, you will be subject to any rules or guidelines
            applicable to those services and they shall be incorporated by reference into these Terms and Conditions.
            Please see our HYPERLINK “https://art-follow.com/privacy.html ” Privacy Policy, which is incorporated
            into these Terms and Conditions by reference.
        </p>
        <h4>Our Service</h4>
        <p>
            Our www.art-follow.com and services provided to you on and through art-follow.com on an “as is” basis.
            You agree that the owners of this www.art-follow.com exclusively reserve the right and may, at any time
            and without notice and any liability to you, modify or discontinue this www.art-follow.com and its services
            or delete the data you provide, whether temporarily or permanently. We shall have no responsibility or
            liability for the timeliness, deletion, failure to store, inaccuracy or improper delivery of any data or
            information.
        </p>
        <h4>Your Responsibilities and Registration Obligations</h4>
        <p>
            In order to use this service, you must register and agree to provide truthful information when requested.
            When registering, you explicitly agree to our Terms and Conditions, which may be modified by us from
            time to time and available here.
        </p>
        <h4>Privacy Policy</h4>
        <p>
            Registration data and other personally identifiable information that we may collect is subject to the terms of
            our HYPERLINK “https://art-follow.com/privacy.html ” Privacy Policy.
        </p>
        <h4>Registrations and Password</h4>
        <p>
            You are responsible to maintain the confidentiality of your password and shall be responsible for all uses
            via your registration and/or login, whether authorized or unauthorized by you. You agree to immediately
            notify us of any unauthorized use or your registration, user account or password.
        </p>
        <h4>Your Conduct</h4>
        <p>
            You agree that art-follow.com may expose you to content that may be objectionable or offensive. We shall
            not be responsible to you in any way for the content that appears on this www.art-follow.com nor for any
            error or omission. You explicitly agree, in using this HYPERLINK “http://www.art-follow.com ” www.artfollow.com
            or any service provided, that you shall not:
            (a) Provide any content or perform any conduct that may be unlawful, illegal, threatening, harmful,
            abusive, harassing, stalking, tortious, defamatory, libelous, vulgar, obscene, offensive, objectionable,
            pornographic, designed to or does interfere or interrupt this www.art-follow.com or any service provided,
            infected with a virus or other destructive or deleterious programming routine, give rise to civil or criminal
            liability, or which may violate an applicable local, national or international law;
            (b) Impersonate or misrepresent your association with any person or entity, or forge or otherwise seek to
            conceal or misrepresent the origin of any content provided by you;
            (c) Collect or harvest any data about other users;
            (d) Provide or use this www.art-follow.com and any content in a manner that would involve junk mail,
            spam, chain letters, pyramid schemes, or any other form of unauthorized advertising without our prior
            written consent;
            (e) Provide any content that may give rise to our civil or criminal liability or which may constitute or be
            considered a violation of any local, national or international law, including but not limited to laws relating
            to copyright, trademark, patent, or trade secrets;
            (f) Infringe on any copyrights or intellectual property rights;
            (g) Offer for sale, promote or advertise through an ad space: weaponry, items designed to restrain or hurt or
            harm or inconvenience people, drugs or narcotics, tobacco, substances which may be used - or advertised as
            such - to achieve hallucinogenic effects, pornography, alcohol, racially offensive, or carry out, promote or
            advertise any manner of illegal activities. art-follow.com reserves the right to report any breaches of this
            nature to the relevant law authorities. www.art-follow.com reserves the right to make judgements upon
            what is permissible to sell through www.art-follow.com Furthermore, you agree that:
            (h) You own the right to publish any and all material you provide www.art-follow.com for use within a
            www.art-follow.com web-store, and will grant www.art-follow.com the rights to use these materials to
            bolster the performance of www.art-follow.com stores. In addition, you agree that you will not:
            (i) Contact customers to request they pay you via means other than the supported www.art-follow.com
            payment providers;
            (j) Try to circumvent any systems www.art-follow.com has in place, including attempting to change free
            stores to remove or obscure the www.art-follow.com advertising bar or other component placed on stores
            by www.art-follow.com
            (k) Log in, or attempt to log in, to a store which is not maintained by yourself.
        </p>
        <h4>Third Party Services</h4>
        <p>
            Goods and services of third parties may be advertised and/or made available on or through this www.artfollow.com.
            Representations made regarding products and services provided by third parties are governed
            by the policies and representations made by these third parties. We shall not be liable for or responsible in
            any manner for any of your dealings or interaction with third parties.
        </p>
        <h4>Indemnification</h4>
        <p>
            You agree to indemnify and hold us harmless, our subsidiaries, affiliates, related parties, officers, directors,
            employees, agents, independent contractors, advertisers, partners, and co-branders from any claim or
            demand, including reasonable legal fees, that may be made by any third party, that is due to or arising out
            of your conduct or connection with this www.art-follow.com or service, your violation of these Terms and
            Conditions or any other violation of the rights of another person or party.
        </p>
        <h4>Disclaimer of Warranties</h4>
        <p>
            You understand and agree that your use of this www.art-follow.com and any services or content provided
            (The “Sevice”) is made available and provided to your at your own risk. It is provided to you “as is” and
            we expressly disclaim all warranties of any kind, implied or expressed, including but not limited to the
            warranties of merchantability, fitness for a particular purpose, and non-infringement. We make no warranty
            that any part of the service will be uninterrupted, error-free, virus-free, timely, secure, accurate, reliable, of
            any quality, nor that any content is safe in any manner for download. You understand and agree that neither
            us nor any participant in the Service provides professional advice of any kind and that use of such advice or
            any other information is solely at your own risk and without our liability of any kind. Some jurisdictions
            may not allow disclaimers of implied warranties and the above disclaimer may not apply to you only as it
            relates to implied warranties.
        </p>
        <h4>Limitation of Liability</h4>
        <P>
            You expressly understand and agree that we shall not be liable for any direct, indirect, special, incidental,
            consequential or exemplary damages, including but not limited to: damages for loss of profits, goodwill,
            use, data or other intangible loss (even if we have been advised of the possibility of such damages),
            resulting from or arising out of; (I) The use of or the inability to use the Service; (II) The cost to obtain
            substitute goods and/or services resulting from any transaction entered into on or through this Service; (III)
            Unauthorized access to or alteration of your data transmissions; (IV) Statements of conduct of any third
            party on the Service, or; (V) Any other matter relating to the Service. In some jurisdictions, it is not
            permitted to limit liability and therefore such limitations may not apply to you.
        </P>
        <h4>Reservation of Rights</h4>
        <p>
            We reserve all of our rights, including but not limited to any and all copyrights, trademarks, patents, trade
            secrets, and any other proprietary right that we may have in our www.art-follow.com, its content, and the
            goods and services that may be provided. The use of our rights and property requires our prior written
            consent. We are not providing you with any implied or express licenses or rights by making services
            available to you and you will have no rights to make any commercial uses of our www.art-follow.com or
            service without our prior written consent.
        </p>
        <h4>Notification of Copyright Infringement</h4>
        <p>
            If you believe that your property has been used in any way that would be considered copyright
            infringement or a violation of your intellectual property rights, please read our DMCA policy by
            HYPERLINK clicking here.
        </p>
        <h4>Applicable Law</h4>
        <p>
            You agree that these Terms and Conditions and any dispute arising out of your use or misuse of this
            www.art-follow.com or our products or services shall be governed by and construed in accordance with
            local laws where the headquarters of the owner of this www.art-follow.com is located, without regard to its
            conflict of law provisions. By registering or using this www.art-follow.com and service you consent and
            submit to the exclusive jurisdiction and venue of the county or city where the headquarters of the owner of
            this www.art-follow.com is located.
        </p>
        <h4> Miscellaneous Information</h4>
        <p>
            (I) In the event that these Terms and Conditions conflicts with any law under which any provision may be
            held invalid by a court with jurisdiction over the parties, such provision will be interpreted to reflect the
            original intentions of the parties in accordance with applicable law, and the remainder of these Terms and
            Conditions will remain valid and intact; (II) The failure of either party to assert any right under these Terms
            and Conditions shall not be considered a waiver of any that party’s right and that right will remain in full
            force and effect; (III) You agree that without regard to any statue or contrary law that any claim or cause
            arising out of this www.art-follow.com or its services must be filed within one (1) year after such claim or
            cause arose or the claim shall be forever barred; (IV) We may assign our rights and obligations under these
            Terms and Conditions and we shall be relieved of any further obligation.
        </p>
        <h4>Consent</h4>
        <p>
            By continuing to browse or otherwise accessing the www.art-follow.com, you signal acceptance of the
            terms and disclaimer set out above. If you do not accept any of these terms, leave this www.art-follow.com
            now. By logging into the www.art-follow.com Control Panel or using any www.art-follow.com you signal
            that you accept these Terms and Conditions in full.
        </p>
        <h4>Content and Materials</h4>
        <p>
            www.art-follow.com may use your www.art-follow.com to show potential clients examples of what we do.
            This includes product images and descriptive text, as well as links to your site. www.art-follow.com may
            help market your items for sale through “showcases” and other product search sites, i.e. Google Product
            Search, in an effort to increase your sales. www.art-follow.com stores contain a small advertising space
            across the the right bottom of each page (The www.art-follow.com Banner) and usually a link to this
            www.art-follow.com. www.art-follow.com reserves the right to use this space to market www.artfollow.com,
            included but not limited to images and HTML controls and elements, its partners and products
            being sold in an effort to bolster www.art-follow.com and its clients. V.I.P users to www.art-follow.com
            may have The www.art-follow.com Banner removed.
        </p>
        <h4>Interpretation</h4>
        <p>
            Where permissible by law, www.art-follow.com is the sole arbiter of the meaning and context of these
            terms and conditions.
        </p>
        <h4>Usage Data</h4>
        <p>
            In the course of managing and optimising stores, www.art-follow.com monitors the activity of sites to
            develop better selling techniques for our clients. This information resides on our servers and is never passed
            on to a third party, and never used for marketing purposes. This information is held purely to provide
            statistical analysis and trace errors, in an effort to benefit our clients.
        </p>
        <h4>Refund Policy</h4>
        <p>
            incase of payment users,Pro-rata refunds can be requested within 5 days of the payment date for monthly
            subscriptions and within 28 days of the payment date for annual subscriptions. All refunds are subject to a 5
            GBP, 6 EUR or 8 USD administration fee. Any days you have already used from the last payment date will
            also be taken from the total refund amount.
        </p>
        <h4>Inactivity</h4>
        <p>
            www.art-follow.com reserves the right to terminate without notice inactive stores. For the purposes of this
            operation, inactive means that we receive complaints about undelivered purchases or a failure to login to a
            store within a length of time determined by www.art-follow.com.
        </p>
        <h4>Requirements</h4>
        <p>
            The Www.art-follow.com Control Panel, primarily located at HYPERLINK “https://
            admin.freewebstore.org/“ https://admin.www.art-follow.com.org/, and available webstore designs are
            designed for use in Microsoft Internet Explorer (versions 7 and up), Mozilla FireFox and Google Chrome.
            Support for other browsers is not necessarily provided.
        </p>
        <h4>Updates</h4>
        <p>
            From time to time, www.art-follow.com servers must be disabled for updates - i.e. important security
            updates or software upgrades. Where possible, notice will be given of this down-time and store fronts will
            be kept open. An example of an update which will receive no notice is a critical security update which
            closes a loophole detrimental to our shoppers’ security or experience.
        </p>
        <h4>Support and Contact Details</h4>
        <p>
            All support is strictly handled online via your www.art-follow.com control panel or by emailing
            support@art-follow.com directly. The www.art-follow.com Ltd registered trading address is balozi estate
            hse. M5 south B ,Nairobi Kenya. For Corporate enquiries please call +254 700 093 493.
        </p>
        <h4>Notice:</h4>
        <p>
            ww.art-follow.com reserves the right to terminate sites without prior notice should the site be selling or
            promoting anything considered by www.art-follow.com unsavory or unsuitable. Complaints against a site
            should be directed to our support department which can be reached by emailing support info@artfollow.com
            or www.art-follow.com will openly work with law enforcement agencies to help with any
            investigations.
            Furthermore, visitors are referred to HYPERLINK “https://www.paypal.com/ga/webapps/mpp/ua/
            acceptableuse-full” PayPal’s Acceptable Use Policy. We will co-operate fully with PayPal to deal with
            www.art-follow.com sites that utilize PayPal as a payment platform, and that fail to comply to these
            standards. Sites that are found in breach of these terms will be terminated without notice. As more Payment
            Providers are added to the bank of companies www.art-follow.com uses, their terms of use become
            incorporated into our terms of use, and site owners must comply with the terms of use of their merchant
            provider. It is the sole responsibility of the client to ensure their store meets their merchant provider’s terms
            and usage policies. Should a store use an alternate payment provider, the store owner (defined as any person
            in control of the credentials to log in to that store’s control panel) must ensure that all goods sold fit in with
            their chosen payment provider’s (or providers’) terms of use. www.art-follow.com operates a policy of
            ethical use determined by the staff at the www.art-follow.com offices. Stores deemed by www.artfollow.com
            to be unsuitable in any respect may be closed without notice.
        </p>
    </div>
@endsection
