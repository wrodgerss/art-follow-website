@extends('master')

@section('title', 'suggestions')

@section('css')
    <link rel="stylesheet" href="{{ asset('static/accounts/css/index.css') }}">
@endsection

@section('content')
    <ul id="suggestions">
        @forelse($users as $user)
            <li>
                <a href="{{ route('accounts.show', $user->username) }}">
                    <img src="{{ $user->profile->avatar_url() }}" alt="{{ $user->username }}" width="162" height="162">
                    <h4 class="user">{{ $user->username }}</h4>
                </a>
                @if($user->workfolios->count() > 0)
                    <h6>
                        {{ $user->workfolios->count() }} {{ str_plural('upload', $user->workfolios->count()) }}
                    </h6>
                @else
                    <h6>no uploads</h6>
                @endif
            </li>
        @empty
            <h1>no suggestions available</h1>
        @endforelse
    </ul>
@endsection
