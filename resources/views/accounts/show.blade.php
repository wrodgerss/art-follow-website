@extends('master')

@section('title')
    profile | {{ $user->username }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('static/accounts/css/show.css') }}">
    <style type="text/css">
        #jumbotron {
            background: linear-gradient(rgba(34, 34, 34, .9), rgba(0, 0, 0, 1)), url({{ $user->profile->avatar_url() }});
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('static/accounts/js/show.js') }}"></script>
@endsection

@section('content')
    <div id="jumbotron">
        @if($user->workfolios->count() > 0)
            <h3 id="uploads-count">
                {{ $user->workfolios->count() }} {{ str_plural('upload', $user->workfolios->count()) }}
            </h3>
        @else
            <h3 id="uploads-count">no uploads</h3>
        @endif
        <h1>{{ $user->username }}</h1>
        @if($user->id == Auth::user()->id)
            <a href="{{ route('accounts.edit') }}" id="edit-account">
                edit
            </a>
        @else
            <form action="{{ route('accounts.connection', $user->username) }}" method="post">
                {{ csrf_field() }}
                <button type="submit">
                    @if($user->connections->contains('id', Auth::user()->id))
                        unfollow
                    @else
                        follow
                    @endif
                </button>
            </form>
        @endif
        <img src="{{ $user->profile->avatar_url() }}" alt="{{ $user->username }}">
        <h4 class="media-heading">
            {{ $user->profile->full_name() }}
        </h4>
        <p id="bio">{{ $user->profile->bio }}</p>
        <h6>
            <i class="fa fa-map-marker"></i>
            {{ $user->profile->location() }}
            &nbsp;<span class="period">.</span>&nbsp;
            {{ $user->profile->age() }} years
            &nbsp;<span class="period">.</span>&nbsp;
            {{ $user->profile->gender }}
        </h6>
    </div>
    <ul id="workfolios">
        @foreach($user->workfolios as $workfolio)
            <li class="workfolio">
                <img src="{{ $workfolio->cover_url() }}" alt="{{ $workfolio->title }}" width="162" height="162">
                <a href="{{ route('workfolio.show', $workfolio->id) }}" data-show="modal">
                    {{ $workfolio->title }}
                </a>
                <small>
                    @if($workfolio->file_type == 'video')
                        video
                    @elseif($workfolio->file_type == 'audio')
                        audio
                    @else
                        image
                    @endif
                </small>
            </li>
        @endforeach
    </ul>
@endsection
