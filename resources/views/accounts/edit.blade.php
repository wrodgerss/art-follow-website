@extends('master')

@section('title', 'edit account')

@section('css')
    <link rel="stylesheet" href="{{ asset('static/accounts/css/forms.css') }}">
@endsection

@section('content')
    @if(session('message'))
        <p class="alert-success">
            {{ session()->get('message') }}
        </p>
    @endif
    @if(!session('newly_registered'))
        <h4 class="heading">account info</h4>
        <form action="{{ route('accounts.change_email') }}" method="post" class="change-account-info">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="input-group">
                <input type="email" name="email" value="{{ $user->email }}" class="form-control">
                <span class="input-group-addon">
                    <button type="submit">
                        change
                    </button>
                </span>
            </div>
            @if($errors->has('email'))
                {{ $errors->first('email') }}
            @endif()
        </form>
        <form action="{{ route('accounts.change_password') }}" method="post" class="change-account-info">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="input-group">
                <input type="password" name="password" placeholder="enter new password" class="form-control">
                <span class="input-group-addon">
                    <button type="submit">
                        change
                    </button>
                </span>
            </div>
            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif()
        </form>
    @endif
    <form action="{{ route('accounts.update') }}" method="post" enctype="multipart/form-data" id="upload-form">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <h4 class="heading">profile info</h4>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    first name
                </span>
                <input type="text" class="form-control" name="first_name" required value="{{ $user->profile->first_name }}">
            </div>
            @if($errors->has('first_name'))
                {{ $errors->first('first_name') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    last name
                </span>
                <input type="text" class="form-control" name="last_name" required value="{{ $user->profile->last_name }}">
            </div>
            @if($errors->has('last_name'))
                {{ $errors->first('last_name') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    country
                </span>
                <input type="text" class="form-control" name="country" required value="{{ $user->profile->country }}">
            </div>
            @if($errors->has('country'))
                {{ $errors->first('country') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    city
                </span>
                <input type="text" class="form-control" name="city" required value="{{ $user->profile->city }}">
            </div>
            @if($errors->has('city'))
                {{ $errors->first('city') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    birth day
                </span>
                <input type="date" class="form-control" name="birth_day" required value="{{ $user->profile->birthdate() }}">
            </div>
            @if($errors->has('birth_day'))
                {{ $errors->first('birth_day') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    avatar
                </span>
                <input type="file" class="form-control" name="avatar">
            </div>
            @if($errors->has('avatar'))
                {{ $errors->first('avatar') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    gender
                </span>
                <div class="form-control">
                    <label>
                        <input type="radio" name="gender" value="male" @if($user->profile->gender == 'male') checked @endif>
                        male
                    </label>
                    <label>
                        <input type="radio" name="gender" value="female" @if($user->profile->gender == 'female') checked @endif>
                        female
                    </label>
                </div>
            </div>
            @if($errors->has('gender'))
                {{ $errors->first('gender') }}
            @endif()
        </div>
        <h4 class="heading">bio</h4>
        <div class="form-group">
            <textarea name="bio" rows="5" class="form-control">
            {{ $user->profile->bio }}
            </textarea>
            @if($errors->has('bio'))
                {{ $errors->first('bio') }}
            @endif()
        </div>
        <button type="submit" class="form-control">
            save
        </button>
    </form>
    <form action="{{ route('accounts.delete') }}" method="post" id="delete-form">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <h5>
            Delete your account
        </h5>
        <button type="submit">
            delete account
        </button>
    </form>
@endsection
