@extends('master')

@section('title', 'upload')

@section('css')
    <link rel="stylesheet" href="{{ asset('static/workfolio/css/forms.css') }}">
@endsection

@section('content')
    <form action="{{ route('workfolio.store') }}" method="post" enctype="multipart/form-data" id="upload-form">
        {{ csrf_field() }}
        <span class="input-group-addon">
            title
        </span>
        <input type="text" class="form-control" name="title" placeholder="title" required autofocus>
        @if($errors->has('title'))
            {{ $errors->first('title') }}
        @endif()

        <span class="input-group-addon">
            cover
        </span>
        <input type="file" class="form-control" name="cover" required>
        @if($errors->has('cover'))
            {{ $errors->first('cover') }}
        @endif()

        <span class="input-group-addon">
            file
        </span>
        <input type="file" class="form-control" name="file_upload" required>
        @if($errors->has('file_upload'))
            {{ $errors->first('file_upload') }}
        @endif()

        <span class="input-group-addon">
            file type
        </span>
        <label>
            <input type="radio" name="file_type" value="video">
            <i class="fa fa-film"></i>
        </label>
        <label>
            <input type="radio" name="file_type" value="audio">
            <i class="fa fa-music"></i>
        </label>
        <label>
            <input type="radio" name="file_type" value="image">
            <i class="fa fa-file-photo-o"></i>
        </label>
        @if($errors->has('file_type'))
            {{ $errors->first('file_type') }}
        @endif()

        <span class="input-group-addon">
            tags
        </span>
        <input type="text" class="form-control" name="tags" placeholder="comma seperated tags (music,video)" required>
        @if($errors->has('tags'))
            {{ $errors->first('tags') }}
        @endif()

        <textarea name="description" rows="5" placeholder="description (optional)" class="form-control"></textarea>
        @if($errors->has('description'))
            {{ $errors->first('description') }}
        @endif()

        <button type="submit" class="form-control">
            upload
        </button>

    </form>
@endsection
