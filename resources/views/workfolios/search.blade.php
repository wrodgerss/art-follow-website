@extends('master')

@section('title')
    search | {{ $lookup }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('static/workfolio/css/search.css') }}">
@endsection

@section('js')
    <script src="{{ asset('static/workfolio/js/list.js') }}"></script>
@endsection

@section('content')
    <ul class="nav nav-pills">
        <li role="presentation" class="active">
            <a href="#videos" role="pill" data-toggle="pill">
                videos
            </a>
        </li>
        <li role="presentation">
            <a href="#audio" role="pill" data-toggle="pill">
                audio
            </a>
        </li>
        <li role="presentation">
            <a href="#images" role="pill" data-toggle="pill">
                images
            </a>
        </li>
        <li role="presentation" role="pill" data-toggle="pill">
            <a href="#people" role="pill" data-toggle="pill">
                people
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="videos">
            <ul class="workfolios">
                @forelse($videos as $workfolio)
                    <li>
                        <img src="{{ $workfolio->cover_url() }}" alt="{{ $workfolio->title }}" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="{{ $workfolio->user->profile->avatar_url() }}" alt="{{ $workfolio->user->username }}" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="{{ route('workfolio.show', $workfolio->id) }}" data-show="modal">
                                    {{ $workfolio->title }}
                                </a>
                            </div>
                        </div>
                        <small>
                            {{ $workfolio->user->username }} 
                        </small>
                    </li>
                @empty
                    <h1>no video found</h1>
                @endforelse
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="audio">
            <ul class="workfolios">
                @forelse($audios as $workfolio)
                    <li>
                        <img src="{{ $workfolio->cover_url() }}" alt="{{ $workfolio->title }}" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="{{ $workfolio->user->profile->avatar_url() }}" alt="{{ $workfolio->user->username }}" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="{{ route('workfolio.show', $workfolio->id) }}" data-show="modal">
                                    {{ $workfolio->title }}
                                </a>
                            </div>
                        </div>
                        <small>
                            {{ $workfolio->user->username }} 
                        </small>
                    </li>
                @empty
                    <h1>no music found</h1>
                @endforelse
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="images">
            <ul class="workfolios">
                @forelse($images as $workfolio)
                    <li>
                        <img src="{{ $workfolio->cover_url() }}" alt="{{ $workfolio->title }}" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="{{ $workfolio->user->profile->avatar_url() }}" alt="{{ $workfolio->user->username }}" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="{{ route('workfolio.show', $workfolio->id) }}" data-show="modal">
                                    {{ $workfolio->title }}
                                </a>
                            </div>
                        </div>
                        <small>
                            {{ $workfolio->user->username }} 
                        </small>
                    </li>
                @empty
                    <h1>no images found</h1>
                @endforelse
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="people">
            <ul>
                @forelse($users as $user)
                    <li>
                        <a href="{{ route('accounts.show', $user->username) }}">
                            <img src="{{ $user->profile->avatar_url() }}" alt="{{ $user->username }}" width="162" height="162">
                            <h4 class="user">
                                {{ $user->username }}
                            </h4>
                        </a>
                        @if($user->workfolios->count() > 0)
                            <h6>
                                {{ $user->workfolios->count() }} upload(s)
                            </h6>
                        @else
                            <h6>no uploads</h6>
                        @endif
                    </li>
                @empty
                    <h1>no user found</h1>
                @endforelse
            </ul>
        </div>
    </div>
@endsection
