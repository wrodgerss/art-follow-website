<div class="media comment">
    <div class="media-left">
        <img src="{{ $comment->user->profile->avatar_url() }}" alt="{{ $comment->user->username }}" class="media-object">
    </div>
    <div class="media-body">
        <div class="media-heading clearfix">
            <span class="user">
            {{ $comment->user->username }}
                <span class="message">
                    -- {{ $comment->message }}
                </span>
            </span>
            <span class="posted">
                {{ $comment->created_at->diffForHumans() }}
            </span>
        </div>
    </div>
</div>
