@extends('master')

@section('title')
    {{ $workfolio->title }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('static/workfolio/css/show.css') }}">
@endsection

@section('js')
    <script src="{{ asset('static/workfolio/js/show.js') }}"></script>
@endsection



@section('content')
    <div id="details-container">
        <div class="media" id="details">
            <div class="media-body">
                <span id="date">
                    {{ $workfolio->created_at->diffForHumans() }}
                </span>
                <h1 id="title">{{ $workfolio->title }}</h1>
                <h5 id="more">
                    {{ $workfolio->views->count() }} view(s)
                    &nbsp;--&nbsp;
                    <span id="likes">
                        {{ $workfolio->likes->count() }}
                    </span>
                    like(s)
                </h5>
                @if(Auth::user()->id == $workfolio->user->id)
                    <a href="{{ route('workfolio.edit', $workfolio->id) }}" id="edit">
                        <i class="fa fa-pencil-square-o"></i> 
                        edit &nbsp; &nbsp; &nbsp;
                    </a>
                @endif
            </div>
            <div class="media-left">
                <img src="{{ $workfolio->cover_url() }}" alt="" class="media-object" id="cover">
            </div>
        </div>
        <div class="media" id="user">
            <div class="media-left">
                <a href="">
                    <img src="{{ $workfolio->user->profile->avatar_url() }}" alt="{{ $workfolio->user->username }}" class="media-object">
                </a>
            </div>
            <div class="media-body">
                <div class="media-heading">
                    <a href="">
                        by {{ $workfolio->user->username }}
                    </a>
                    <a href="{{ route('workfolio.like', $workfolio->id) }}" id="like">
                        <i class="fa fa-heart"></i> &nbsp;like
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="tabs">
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active">
                <a href="#media-content" role="pill" data-toggle="pill">
                    @if($workfolio->file_type == 'video' || $workfolio->file_type == 'audio')
                        play
                    @else
                        view
                    @endif
                </a>
            </li>
            <li role="presentation">
                <a href="#description" role="pill" data-toggle="pill">
                    description
                </a>
            </li>
            <li role="presentation">
                <a href="#comments" role="pill" data-toggle="pill">
                    comments
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="media-content">
                @if($workfolio->file_type == 'video')
                    @include('workfolios.partials.video', ['video' => $workfolio->file_url()])
                @elseif($workfolio->file_type == 'image')
                    @include('workfolios.partials.image', ['image' => $workfolio->file_url()])
                @else
                    @include('workfolios.partials.audio', ['audio' => $workfolio->file_url()])
                @endif
            </div>
            <div role="tabpanel" class="tab-pane" id="description">
                @if($workfolio->description != null)
                    <p>{{ $workfolio->description }}</p>
                @else
                    <p>No description available</p>
                @endif
            </div>
            <div role="tabpanel" class="tab-pane" id="comments">
                <div data-url="{{ route('workfolio.comment', $workfolio->id) }}" id="comment-form">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-smile-o"></i>
                        </span>
                        <input type="text" name="comment" class="form-control" placeholder="leave a comment" required>
                        <span class="input-group-addon" id="post">
                            post
                        </span>
                    </div>
                </div>
                @foreach($workfolio->comments as $comment)
                    @include('workfolios.partials.comment', ['comment' => $comment])
                @endforeach
            </div>
        </div>
    </div>
@endsection
