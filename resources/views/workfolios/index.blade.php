@extends('master')

@section('title', 'home')

@section('css')
    <link rel="stylesheet" href="{{ asset('static/workfolio/css/index.css') }}">
@endsection

@section('js')
    <script src="{{ asset('static/workfolio/js/list.js') }}"></script>
@endsection

@section('content')
    <ul id="workfolios">
        @forelse($workfolios as $workfolio)
            <li class="workfolio">
                <!-- cover -->
                <img src="{{ $workfolio->cover_url() }}" alt="{{ $workfolio->title }}" width="162" height="162">
                <!-- user -->
                <div class="media details">
                    <div class="media-left">
                        <a href="">
                            <img src="{{ $workfolio->user->profile->avatar_url() }}" alt="{{ $workfolio->user->username }}" class="media-object">
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="{{ route('workfolio.show', $workfolio->id) }}">
                            {{ $workfolio->title }}
                        </a>
                    </div>
                </div>
                <small>
                    {{ $workfolio->user->username }} 
                </small>
            </li>
        @empty
            <h1 class="text-center">no uploads currently available</h1>
        @endforelse
    </ul>
    <!-- show workfolio modal -->
    <div class="modal fade" id="show-project" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection
