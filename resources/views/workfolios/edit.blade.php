@extends('master')

@section('title', 'upload')

@section('css')
    <link rel="stylesheet" href="{{ asset('static/workfolio/css/forms.css') }}">
@endsection

@section('content')
    <form action="{{ route('workfolio.update', $workfolio->id) }}" method="post" enctype="multipart/form-data" id="upload-form">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        @if(session('message'))
            <p class="alert-success">
                {{ session()->get('message') }}
            </p>
        @endif
        <h4 class="heading">upload info</h4>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    title
                </span>
                <input type="text" class="form-control" name="title" placeholder="title" required value="{{ $workfolio->title }}">
            </div>
            @if($errors->has('title'))
                {{ $errors->first('title') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    cover
                </span>
                <input type="file" class="form-control" name="cover" required>
            </div>
            @if($errors->has('cover'))
                {{ $errors->first('cover') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    file
                </span>
                <input type="file" class="form-control" name="file_upload" required>
            </div>
            @if($errors->has('file_upload'))
                {{ $errors->first('file_upload') }}
            @endif()
            <div class="input-group">
                <span class="input-group-addon">
                    file type
                </span>
                <div class="form-control">
                    <label>
                        <input type="radio" name="file_type" value="video" @if($workfolio->file_type == 'video') checked @endif>
                        <i class="fa fa-film"></i>
                    </label>
                    <label>
                        <input type="radio" name="file_type" value="audio" @if($workfolio->file_type == 'audio') checked @endif>
                        <i class="fa fa-music"></i>
                    </label>
                    <label>
                        <input type="radio" name="file_type" value="image" @if($workfolio->file_type == 'image') checked @endif>
                        <i class="fa fa-file-photo-o"></i>
                    </label>
                </div>
            </div>
            @if($errors->has('file_type'))
                {{ $errors->first('file_type') }}
            @endif()
        </div>
        <h4 class="heading">upload overview</h4>
        <div class="form-group">
            <textarea name="description" rows="5" placeholder="description (optional)" class="form-control">
            {{ $workfolio->description }}
            </textarea>
            @if($errors->has('description'))
                {{ $errors->first('description') }}
            @endif()
        </div>
        <button type="submit" class="form-control">
            save
        </button>
    </form>
    <form action="{{ route('workfolio.delete', $workfolio->id) }}" method="post" id="delete-form">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <h5>
            Delete this upload from your portfolio
        </h5>
        <button type="submit">
            delete
        </button>
    </form>
@endsection
