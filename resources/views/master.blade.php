<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- x-csrf-token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Art-follow | @yield('title')</title>
        <!-- jquery -->
        <script src="{{ asset('vendor/jquery.js') }}"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- font awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- stroke 7 icons -->
        <link rel="stylesheet" href="{{ asset('vendor/Icon-font-7-stroke/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/Icon-font-7-stroke/pe-icon-7-stroke/css/helper.css') }}">
        <!-- css -->
        <link rel="stylesheet" href="{{ asset('static/master/css/style.css') }}">
        @yield('css')
    </head>

    <body>
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="" class="navbar-brand">
                        Art-foll<span>o</span>w
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <a href="{{ route('workfolio.create') }}" class="btn navbar-btn navbar-right">
                        upload
                    </a>
                    <form action="" class="navbar-form navbar-right">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="pe-7s-search"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Search workfolio, members...">
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="{{ route('workfolio.index') }}">
                                workfolio
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('accounts.index') }}">
                                suggestions
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="{{ route('accounts.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                                account
                            </a>
                            <i class="pe-7s-angle-down"></i>
                            <ul class="dropdown-menu">
                                <li>
                                    <a id="signout" class="pe-7s-back-2">
                                        sign out
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <a href="" class="navbar-text navbar-right pe-7s-bell"></a>
            </div>
        </nav>

        <div id="content">
            @yield('content')
        </div>

        <!-- js -->
        <script src="{{ asset('static/master/js/script.js') }}"></script>

        <script>

            window.Laravel.userId = <?php echo auth()->user()->id; ?>
            $(document).ready(function() {

                $.get('/notifications', function (data) {

                    // show notiications
                });
            });

            window.Echo = new Echo({
                broadcaster: 'pusher',
                key: 'your-pusher-key'
            });

            window.Echo.private('App.User.' + Laravel.userId).notification( function (notification) {

                // add notiication
            });


        </script>
        
        @yield('js')
    </body>

</html>