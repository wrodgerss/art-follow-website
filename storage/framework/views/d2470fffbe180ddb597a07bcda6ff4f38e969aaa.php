<?php $__env->startSection('title', 'upload'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/workfolio/css/forms.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <form action="<?php echo e(route('workfolio.store')); ?>" method="post" enctype="multipart/form-data" id="upload-form">
        <?php echo e(csrf_field()); ?>

        <span class="input-group-addon">
            title
        </span>
        <input type="text" class="form-control" name="title" placeholder="title" required autofocus>
        <?php if($errors->has('title')): ?>
            <?php echo e($errors->first('title')); ?>

        <?php endif; ?>

        <span class="input-group-addon">
            cover
        </span>
        <input type="file" class="form-control" name="cover" required>
        <?php if($errors->has('cover')): ?>
            <?php echo e($errors->first('cover')); ?>

        <?php endif; ?>

        <span class="input-group-addon">
            file
        </span>
        <input type="file" class="form-control" name="file_upload" required>
        <?php if($errors->has('file_upload')): ?>
            <?php echo e($errors->first('file_upload')); ?>

        <?php endif; ?>

        <span class="input-group-addon">
            file type
        </span>
        <label>
            <input type="radio" name="file_type" value="video">
            <i class="fa fa-film"></i>
        </label>
        <label>
            <input type="radio" name="file_type" value="audio">
            <i class="fa fa-music"></i>
        </label>
        <label>
            <input type="radio" name="file_type" value="image">
            <i class="fa fa-file-photo-o"></i>
        </label>
        <?php if($errors->has('file_type')): ?>
            <?php echo e($errors->first('file_type')); ?>

        <?php endif; ?>

        <span class="input-group-addon">
            tags
        </span>
        <input type="text" class="form-control" name="tags" placeholder="comma seperated tags (music,video)" required>
        <?php if($errors->has('tags')): ?>
            <?php echo e($errors->first('tags')); ?>

        <?php endif; ?>

        <textarea name="description" rows="5" placeholder="description (optional)" class="form-control"></textarea>
        <?php if($errors->has('description')): ?>
            <?php echo e($errors->first('description')); ?>

        <?php endif; ?>

        <button type="submit" class="form-control">
            upload
        </button>

    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>