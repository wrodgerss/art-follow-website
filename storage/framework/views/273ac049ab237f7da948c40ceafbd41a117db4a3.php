<?php $__env->startSection('title'); ?>
    search | <?php echo e($lookup); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/workfolio/css/search.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('static/workfolio/js/list.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <ul class="nav nav-pills">
        <li role="presentation" class="active">
            <a href="#videos" role="pill" data-toggle="pill">
                videos
            </a>
        </li>
        <li role="presentation">
            <a href="#audio" role="pill" data-toggle="pill">
                audio
            </a>
        </li>
        <li role="presentation">
            <a href="#images" role="pill" data-toggle="pill">
                images
            </a>
        </li>
        <li role="presentation" role="pill" data-toggle="pill">
            <a href="#people" role="pill" data-toggle="pill">
                people
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="videos">
            <ul class="workfolios">
                <?php $__empty_1 = true; $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <li>
                        <img src="<?php echo e($workfolio->cover_url()); ?>" alt="<?php echo e($workfolio->title); ?>" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="<?php echo e($workfolio->user->profile->avatar_url()); ?>" alt="<?php echo e($workfolio->user->username); ?>" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="<?php echo e(route('workfolio.show', $workfolio->id)); ?>" data-show="modal">
                                    <?php echo e($workfolio->title); ?>

                                </a>
                            </div>
                        </div>
                        <small>
                            <?php echo e($workfolio->user->username); ?> 
                        </small>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <h1>no video found</h1>
                <?php endif; ?>
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="audio">
            <ul class="workfolios">
                <?php $__empty_1 = true; $__currentLoopData = $audios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <li>
                        <img src="<?php echo e($workfolio->cover_url()); ?>" alt="<?php echo e($workfolio->title); ?>" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="<?php echo e($workfolio->user->profile->avatar_url()); ?>" alt="<?php echo e($workfolio->user->username); ?>" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="<?php echo e(route('workfolio.show', $workfolio->id)); ?>" data-show="modal">
                                    <?php echo e($workfolio->title); ?>

                                </a>
                            </div>
                        </div>
                        <small>
                            <?php echo e($workfolio->user->username); ?> 
                        </small>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <h1>no music found</h1>
                <?php endif; ?>
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="images">
            <ul class="workfolios">
                <?php $__empty_1 = true; $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <li>
                        <img src="<?php echo e($workfolio->cover_url()); ?>" alt="<?php echo e($workfolio->title); ?>" width="162" height="162">
                        <div class="media details">
                            <div class="media-left">
                                <img src="<?php echo e($workfolio->user->profile->avatar_url()); ?>" alt="<?php echo e($workfolio->user->username); ?>" class="media-object">
                            </div>
                            <div class="media-body">
                                <a href="<?php echo e(route('workfolio.show', $workfolio->id)); ?>" data-show="modal">
                                    <?php echo e($workfolio->title); ?>

                                </a>
                            </div>
                        </div>
                        <small>
                            <?php echo e($workfolio->user->username); ?> 
                        </small>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <h1>no images found</h1>
                <?php endif; ?>
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="people">
            <ul>
                <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <li>
                        <a href="<?php echo e(route('accounts.show', $user->username)); ?>">
                            <img src="<?php echo e($user->profile->avatar_url()); ?>" alt="<?php echo e($user->username); ?>" width="162" height="162">
                            <h4 class="user">
                                <?php echo e($user->username); ?>

                            </h4>
                        </a>
                        <?php if($user->workfolios->count() > 0): ?>
                            <h6>
                                <?php echo e($user->workfolios->count()); ?> upload(s)
                            </h6>
                        <?php else: ?>
                            <h6>no uploads</h6>
                        <?php endif; ?>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <h1>no user found</h1>
                <?php endif; ?>
            </ul>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>