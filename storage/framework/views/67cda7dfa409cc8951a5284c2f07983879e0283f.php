<?php $__env->startSection('title', 'home'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/workfolio/css/index.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('static/workfolio/js/list.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <ul id="workfolios">
        <?php $__empty_1 = true; $__currentLoopData = $workfolios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <li class="workfolio">
                <!-- cover -->
                <img src="<?php echo e($workfolio->cover_url()); ?>" alt="<?php echo e($workfolio->title); ?>" width="162" height="162">
                <!-- user -->
                <div class="media details">
                    <div class="media-left">
                        <a href="">
                            <img src="<?php echo e($workfolio->user->profile->avatar_url()); ?>" alt="<?php echo e($workfolio->user->username); ?>" class="media-object">
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="<?php echo e(route('workfolio.show', $workfolio->id)); ?>">
                            <?php echo e($workfolio->title); ?>

                        </a>
                    </div>
                </div>
                <small>
                    <?php echo e($workfolio->user->username); ?> 
                </small>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <h1 class="text-center">no uploads currently available</h1>
        <?php endif; ?>
    </ul>
    <!-- show workfolio modal -->
    <div class="modal fade" id="show-project" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>