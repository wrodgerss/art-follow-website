<?php $__env->startSection('title', 'upload'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/workfolio/css/forms.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <form action="<?php echo e(route('workfolio.update', $workfolio->id)); ?>" method="post" enctype="multipart/form-data" id="upload-form">
        <?php echo e(method_field('PUT')); ?>

        <?php echo e(csrf_field()); ?>

        <?php if(session('message')): ?>
            <p class="alert-success">
                <?php echo e(session()->get('message')); ?>

            </p>
        <?php endif; ?>
        <h4 class="heading">upload info</h4>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    title
                </span>
                <input type="text" class="form-control" name="title" placeholder="title" required value="<?php echo e($workfolio->title); ?>">
            </div>
            <?php if($errors->has('title')): ?>
                <?php echo e($errors->first('title')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    cover
                </span>
                <input type="file" class="form-control" name="cover" required>
            </div>
            <?php if($errors->has('cover')): ?>
                <?php echo e($errors->first('cover')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    file
                </span>
                <input type="file" class="form-control" name="file_upload" required>
            </div>
            <?php if($errors->has('file_upload')): ?>
                <?php echo e($errors->first('file_upload')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    file type
                </span>
                <div class="form-control">
                    <label>
                        <input type="radio" name="file_type" value="video" <?php if($workfolio->file_type == 'video'): ?> checked <?php endif; ?>>
                        <i class="fa fa-film"></i>
                    </label>
                    <label>
                        <input type="radio" name="file_type" value="audio" <?php if($workfolio->file_type == 'audio'): ?> checked <?php endif; ?>>
                        <i class="fa fa-music"></i>
                    </label>
                    <label>
                        <input type="radio" name="file_type" value="image" <?php if($workfolio->file_type == 'image'): ?> checked <?php endif; ?>>
                        <i class="fa fa-file-photo-o"></i>
                    </label>
                </div>
            </div>
            <?php if($errors->has('file_type')): ?>
                <?php echo e($errors->first('file_type')); ?>

            <?php endif; ?>
        </div>
        <h4 class="heading">upload overview</h4>
        <div class="form-group">
            <textarea name="description" rows="5" placeholder="description (optional)" class="form-control">
            <?php echo e($workfolio->description); ?>

            </textarea>
            <?php if($errors->has('description')): ?>
                <?php echo e($errors->first('description')); ?>

            <?php endif; ?>
        </div>
        <button type="submit" class="form-control">
            save
        </button>
    </form>
    <form action="<?php echo e(route('workfolio.delete', $workfolio->id)); ?>" method="post" id="delete-form">
        <?php echo e(method_field('DELETE')); ?>

        <?php echo e(csrf_field()); ?>

        <h5>
            Delete this upload from your portfolio
        </h5>
        <button type="submit">
            delete
        </button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>