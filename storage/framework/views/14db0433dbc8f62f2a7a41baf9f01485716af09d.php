<?php $__env->startSection('title', 'edit account'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/accounts/css/forms.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if(session('message')): ?>
        <p class="alert-success">
            <?php echo e(session()->get('message')); ?>

        </p>
    <?php endif; ?>
    <?php if(!session('newly_registered')): ?>
        <h4 class="heading">account info</h4>
        <form action="<?php echo e(route('accounts.change_email')); ?>" method="post" class="change-account-info">
            <?php echo e(method_field('PATCH')); ?>

            <?php echo e(csrf_field()); ?>

            <div class="input-group">
                <input type="email" name="email" value="<?php echo e($user->email); ?>" class="form-control">
                <span class="input-group-addon">
                    <button type="submit">
                        change
                    </button>
                </span>
            </div>
            <?php if($errors->has('email')): ?>
                <?php echo e($errors->first('email')); ?>

            <?php endif; ?>
        </form>
        <form action="<?php echo e(route('accounts.change_password')); ?>" method="post" class="change-account-info">
            <?php echo e(method_field('PATCH')); ?>

            <?php echo e(csrf_field()); ?>

            <div class="input-group">
                <input type="password" name="password" placeholder="enter new password" class="form-control">
                <span class="input-group-addon">
                    <button type="submit">
                        change
                    </button>
                </span>
            </div>
            <?php if($errors->has('password')): ?>
                <?php echo e($errors->first('password')); ?>

            <?php endif; ?>
        </form>
    <?php endif; ?>
    <form action="<?php echo e(route('accounts.update')); ?>" method="post" enctype="multipart/form-data" id="upload-form">
        <?php echo e(method_field('PUT')); ?>

        <?php echo e(csrf_field()); ?>

        <h4 class="heading">profile info</h4>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    first name
                </span>
                <input type="text" class="form-control" name="first_name" required value="<?php echo e($user->profile->first_name); ?>">
            </div>
            <?php if($errors->has('first_name')): ?>
                <?php echo e($errors->first('first_name')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    last name
                </span>
                <input type="text" class="form-control" name="last_name" required value="<?php echo e($user->profile->last_name); ?>">
            </div>
            <?php if($errors->has('last_name')): ?>
                <?php echo e($errors->first('last_name')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    country
                </span>
                <input type="text" class="form-control" name="country" required value="<?php echo e($user->profile->country); ?>">
            </div>
            <?php if($errors->has('country')): ?>
                <?php echo e($errors->first('country')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    city
                </span>
                <input type="text" class="form-control" name="city" required value="<?php echo e($user->profile->city); ?>">
            </div>
            <?php if($errors->has('city')): ?>
                <?php echo e($errors->first('city')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    birth day
                </span>
                <input type="date" class="form-control" name="birth_day" required value="<?php echo e($user->profile->birthdate()); ?>">
            </div>
            <?php if($errors->has('birth_day')): ?>
                <?php echo e($errors->first('birth_day')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    avatar
                </span>
                <input type="file" class="form-control" name="avatar">
            </div>
            <?php if($errors->has('avatar')): ?>
                <?php echo e($errors->first('avatar')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    gender
                </span>
                <div class="form-control">
                    <label>
                        <input type="radio" name="gender" value="male" <?php if($user->profile->gender == 'male'): ?> checked <?php endif; ?>>
                        male
                    </label>
                    <label>
                        <input type="radio" name="gender" value="female" <?php if($user->profile->gender == 'female'): ?> checked <?php endif; ?>>
                        female
                    </label>
                </div>
            </div>
            <?php if($errors->has('gender')): ?>
                <?php echo e($errors->first('gender')); ?>

            <?php endif; ?>
        </div>
        <h4 class="heading">bio</h4>
        <div class="form-group">
            <textarea name="bio" rows="5" class="form-control">
            <?php echo e($user->profile->bio); ?>

            </textarea>
            <?php if($errors->has('bio')): ?>
                <?php echo e($errors->first('bio')); ?>

            <?php endif; ?>
        </div>
        <button type="submit" class="form-control">
            save
        </button>
    </form>
    <form action="<?php echo e(route('accounts.delete')); ?>" method="post" id="delete-form">
        <?php echo e(method_field('DELETE')); ?>

        <?php echo e(csrf_field()); ?>

        <h5>
            Delete your account
        </h5>
        <button type="submit">
            delete account
        </button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>