<?php $__env->startSection('title'); ?>
    profile | <?php echo e($user->username); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/accounts/css/show.css')); ?>">
    <style type="text/css">
        #jumbotron {
            background: linear-gradient(rgba(34, 34, 34, .9), rgba(0, 0, 0, 1)), url(<?php echo e($user->profile->avatar_url()); ?>);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('static/accounts/js/show.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div id="jumbotron">
        <?php if($user->workfolios->count() > 0): ?>
            <h3 id="uploads-count">
                <?php echo e($user->workfolios->count()); ?> <?php echo e(str_plural('upload', $user->workfolios->count())); ?>

            </h3>
        <?php else: ?>
            <h3 id="uploads-count">no uploads</h3>
        <?php endif; ?>
        <h1><?php echo e($user->username); ?></h1>
        <?php if($user->id == Auth::user()->id): ?>
            <a href="<?php echo e(route('accounts.edit')); ?>" id="edit-account">
                edit
            </a>
        <?php else: ?>
            <form action="<?php echo e(route('accounts.connection', $user->username)); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <button type="submit">
                    <?php if($user->connections->contains('id', Auth::user()->id)): ?>
                        unfollow
                    <?php else: ?>
                        follow
                    <?php endif; ?>
                </button>
            </form>
        <?php endif; ?>
        <img src="<?php echo e($user->profile->avatar_url()); ?>" alt="<?php echo e($user->username); ?>">
        <h4 class="media-heading">
            <?php echo e($user->profile->full_name()); ?>

        </h4>
        <p id="bio"><?php echo e($user->profile->bio); ?></p>
        <h6>
            <i class="fa fa-map-marker"></i>
            <?php echo e($user->profile->location()); ?>

            &nbsp;<span class="period">.</span>&nbsp;
            <?php echo e($user->profile->age()); ?> years
            &nbsp;<span class="period">.</span>&nbsp;
            <?php echo e($user->profile->gender); ?>

        </h6>
    </div>
    <ul id="workfolios">
        <?php $__currentLoopData = $user->workfolios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="workfolio">
                <img src="<?php echo e($workfolio->cover_url()); ?>" alt="<?php echo e($workfolio->title); ?>" width="162" height="162">
                <a href="<?php echo e(route('workfolio.show', $workfolio->id)); ?>" data-show="modal">
                    <?php echo e($workfolio->title); ?>

                </a>
                <small>
                    <?php if($workfolio->file_type == 'video'): ?>
                        video
                    <?php elseif($workfolio->file_type == 'audio'): ?>
                        audio
                    <?php else: ?>
                        image
                    <?php endif; ?>
                </small>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>