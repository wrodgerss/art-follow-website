<?php $__env->startSection('title'); ?>
    <?php echo e($workfolio->title); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/workfolio/css/show.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('static/workfolio/js/show.js')); ?>"></script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <div id="details-container">
        <div class="media" id="details">
            <div class="media-body">
                <span id="date">
                    <?php echo e($workfolio->created_at->diffForHumans()); ?>

                </span>
                <h1 id="title"><?php echo e($workfolio->title); ?></h1>
                <h5 id="more">
                    <?php echo e($workfolio->views->count()); ?> view(s)
                    &nbsp;--&nbsp;
                    <span id="likes">
                        <?php echo e($workfolio->likes->count()); ?>

                    </span>
                    like(s)
                </h5>
                <?php if(Auth::user()->id == $workfolio->user->id): ?>
                    <a href="<?php echo e(route('workfolio.edit', $workfolio->id)); ?>" id="edit">
                        <i class="fa fa-pencil-square-o"></i> 
                        edit &nbsp; &nbsp; &nbsp;
                    </a>
                <?php endif; ?>
            </div>
            <div class="media-left">
                <img src="<?php echo e($workfolio->cover_url()); ?>" alt="" class="media-object" id="cover">
            </div>
        </div>
        <div class="media" id="user">
            <div class="media-left">
                <a href="">
                    <img src="<?php echo e($workfolio->user->profile->avatar_url()); ?>" alt="<?php echo e($workfolio->user->username); ?>" class="media-object">
                </a>
            </div>
            <div class="media-body">
                <div class="media-heading">
                    <a href="">
                        by <?php echo e($workfolio->user->username); ?>

                    </a>
                    <a href="<?php echo e(route('workfolio.like', $workfolio->id)); ?>" id="like">
                        <i class="fa fa-heart"></i> &nbsp;like
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="tabs">
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active">
                <a href="#media-content" role="pill" data-toggle="pill">
                    <?php if($workfolio->file_type == 'video' || $workfolio->file_type == 'audio'): ?>
                        play
                    <?php else: ?>
                        view
                    <?php endif; ?>
                </a>
            </li>
            <li role="presentation">
                <a href="#description" role="pill" data-toggle="pill">
                    description
                </a>
            </li>
            <li role="presentation">
                <a href="#comments" role="pill" data-toggle="pill">
                    comments
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="media-content">
                <?php if($workfolio->file_type == 'video'): ?>
                    <?php echo $__env->make('workfolios.partials.video', ['video' => $workfolio->file_url()], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php elseif($workfolio->file_type == 'image'): ?>
                    <?php echo $__env->make('workfolios.partials.image', ['image' => $workfolio->file_url()], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php else: ?>
                    <?php echo $__env->make('workfolios.partials.audio', ['audio' => $workfolio->file_url()], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="description">
                <?php if($workfolio->description != null): ?>
                    <p><?php echo e($workfolio->description); ?></p>
                <?php else: ?>
                    <p>No description available</p>
                <?php endif; ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="comments">
                <div data-url="<?php echo e(route('workfolio.comment', $workfolio->id)); ?>" id="comment-form">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-smile-o"></i>
                        </span>
                        <input type="text" name="comment" class="form-control" placeholder="leave a comment" required>
                        <span class="input-group-addon" id="post">
                            post
                        </span>
                    </div>
                </div>
                <?php $__currentLoopData = $workfolio->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo $__env->make('workfolios.partials.comment', ['comment' => $comment], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>