<?php $__env->startSection('title', 'sign up'); ?>

<?php $__env->startSection('content'); ?>
    <h1 id="title">register</h1>
    <form action="<?php echo e(route('register')); ?>" method="post">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" class="form-control" name="username" placeholder="username">
            </div>
            <?php if($errors->has('username')): ?>
                <?php echo e($errors->first('username')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                </span>
                <input type="email" class="form-control" name="email" placeholder="email">
            </div>
            <?php if($errors->has('email')): ?>
                <?php echo e($errors->first('email')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password" placeholder="password">
            </div>
            <?php if($errors->has('password')): ?>
                <?php echo e($errors->first('password')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password_confirmation" placeholder="confirm password">
            </div>
        </div>
        <button type="submit" class="form-control">
            sign up
        </button>
        <hr>
        Already have an account? 
        <a href="<?php echo e(route('login')); ?>">
            sign in
        </a>
        here
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>