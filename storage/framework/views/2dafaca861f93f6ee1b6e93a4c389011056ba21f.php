<?php $__env->startSection('title', 'sign in'); ?>

<?php $__env->startSection('content'); ?>
    <h1 id="title">login</h1>
    <form action="<?php echo e(route('login')); ?>" method="post">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" class="form-control" name="username" placeholder="username">
            </div>
            <?php if($errors->has('username')): ?>
                <?php echo e($errors->first('username')); ?>

            <?php endif; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                <input type="password" class="form-control" name="password" placeholder="password">
                <span class="input-group-addon" id="forgot-password">
                    <a href="">
                        Forgot password?
                    </a>
                </span>
            </div>
            <?php if($errors->has('password')): ?>
                <?php echo e($errors->first('password')); ?>

            <?php endif; ?>
        </div>
        <div class="checkbox">
            <label>
            <input type="checkbox" name="remember"> Remember me
            </label>
        </div>
        <button type="submit" class="form-control">
            sign in
        </button>
        <hr>
        Don't have an account? 
        <a href="<?php echo e(route('register')); ?>">
            sign up
        </a>
        here
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>