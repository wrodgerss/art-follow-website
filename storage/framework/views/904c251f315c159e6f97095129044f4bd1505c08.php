<?php $__env->startSection('title', 'suggestions'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('static/accounts/css/index.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <ul id="suggestions">
        <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <li>
                <a href="<?php echo e(route('accounts.show', $user->username)); ?>">
                    <img src="<?php echo e($user->profile->avatar_url()); ?>" alt="<?php echo e($user->username); ?>" width="162" height="162">
                    <h4 class="user"><?php echo e($user->username); ?></h4>
                </a>
                <?php if($user->workfolios->count() > 0): ?>
                    <h6>
                        <?php echo e($user->workfolios->count()); ?> <?php echo e(str_plural('upload', $user->workfolios->count())); ?>

                    </h6>
                <?php else: ?>
                    <h6>no uploads</h6>
                <?php endif; ?>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <h1>no suggestions available</h1>
        <?php endif; ?>
    </ul>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>