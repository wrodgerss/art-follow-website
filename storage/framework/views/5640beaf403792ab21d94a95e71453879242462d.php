<div class="media comment">
    <div class="media-left">
        <img src="<?php echo e($comment->user->profile->avatar_url()); ?>" alt="<?php echo e($comment->user->username); ?>" class="media-object">
    </div>
    <div class="media-body">
        <div class="media-heading clearfix">
            <span class="user">
            <?php echo e($comment->user->username); ?>

                <span class="message">
                    -- <?php echo e($comment->message); ?>

                </span>
            </span>
            <span class="posted">
                <?php echo e($comment->created_at->diffForHumans()); ?>

            </span>
        </div>
    </div>
</div>
