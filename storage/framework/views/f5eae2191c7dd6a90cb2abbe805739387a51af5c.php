<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- x-csrf-token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <title>Art-follow | <?php echo $__env->yieldContent('title'); ?></title>
        <!-- jquery -->
        <script src="<?php echo e(asset('vendor/jquery.js')); ?>"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>">
        <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
        <!-- font awesome -->
        <link rel="stylesheet" href="<?php echo e(asset('vendor/font-awesome/css/font-awesome.min.css')); ?>">
        <!-- css -->
        <link rel="stylesheet" href="<?php echo e(asset('static/auth/css/forms.css')); ?>">
        <?php echo $__env->yieldContent('css'); ?>
    </head>

    <body>
        <?php echo $__env->yieldContent('content'); ?>
    </body>

</html>