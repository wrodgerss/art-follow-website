<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('terms-and-conditions', function () {

    return view('terms_and_conditions');
});

Auth::routes();

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');

Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::middleware('auth')->group(function () {

    Route::prefix('account')->group(function () {

        Route::get('/', 'AccountsController@index')->name('accounts.index');
        Route::get('{username}/profile', 'AccountsController@show')->name('accounts.show');
        Route::get('edit', 'AccountsController@edit')->name('accounts.edit');
        Route::put('update', 'AccountsController@update')->name('accounts.update');
        Route::post('{username}/connection', 'AccountsController@connection')->name('accounts.connection');
        Route::delete('/', 'AccountsController@destroy')->name('accounts.delete');
        Route::patch('change-password', 'AccountsController@change_password')->name('accounts.change_password');
        Route::patch('change-email', 'AccountsController@change_email')->name('accounts.change_email');

        Route::get('notifications', 'AccountsController@notifications');
    });

    Route::prefix('workfolio')->group(function () {
        
        Route::get('{type}/filter', 'WorkfolioController@filter')->name('workfolio.filter');
        Route::get('/', 'WorkfolioController@index')->name('workfolio.index');
        Route::post('/', 'WorkfolioController@store')->name('workfolio.store');
        Route::get('create', 'WorkfolioController@create')->name('workfolio.create');
        Route::get('search', 'WorkfolioController@search')->name('workfolio.search');
        Route::get('{workfolio}/show', 'WorkfolioController@show')->name('workfolio.show');
        Route::get('{workfolio}/edit', 'WorkfolioController@edit')->name('workfolio.edit');
        Route::put('{workfolio}/update', 'WorkfolioController@update')->name('workfolio.update');
        Route::delete('{workfolio}/delete', 'WorkfolioController@destroy')->name('workfolio.delete');
        Route::post('{workfolio}/comment', 'WorkfolioController@comment')->name('workfolio.comment');
        Route::post('{workfolio}/like', 'WorkfolioController@like')->name('workfolio.like');

        /** Rating */
        Route::post('{workfolio}/rating', 'WorkfolioController@rating')->name('workfolio.rating');

        /** Create participant form */
        Route::get('{workfolio}/participant/create', 'UserTaggingController@create')->name('participant.create');

        /** Store participant */
        Route::post('{workfolio}/participant', 'WorkfolioController@participant')->name('participant.store');

        /** Edit participant form */
        Route::get('{workfolio}/participant/{user}/edit', 'UserTaggingController@edit')->name('participant.edit');

        /** Update participant */
        Route::put('{workfolio}/participant/update', 'UserTaggingController@update')->name('participant.update');

        /** Remove participant */
        Route::delete('{workfolio}/participant/remove', 'UserTaggingController@destroy')->name('participant.remove');

        /** Create gallery form */
        Route::get('{workfolio}/gallery/create', 'GalleryController@create')->name('gallery.create');

        /** Store gallery */
        Route::post('{workfolio}/gallery', 'WorkfolioController@gallery')->name('workfolio.gallery')->name('gallery.store');

        /** Show gallery */
        Route::get('gallery/{gallery}/show', 'GalleryController@show')->name('gallery.show');

        /** Edit gallery form */
        Route::get('gallery/{gallery}/edit', 'GalleryController@edit')->name('gallery.edit');

        /** Update gallery */
        Route::put('gallery/{gallery}/update', 'GalleryController@update')->name('gallery.update');

        /** Delete gallery */
        Route::delete('gallery/{gallery}/delete', 'GalleryController@destroy')->name('gallery.delete');
    });
});
